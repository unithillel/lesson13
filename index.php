<?php

require_once 'classes/BlogEntry.php';
require_once 'classes/Film.php';

$film = new Film('Lord of Rings', 'Fantasy', '10');
$blogPost = new BlogEntry('Php is awesome', 'somthing about php', 'content about PHP');

echo '<pre>';
print_r($film);
print_r($film->toArray());
print_r($film->toJson());
echo '<br>';
print_r($blogPost);
print_r($blogPost->toArray());
print_r($blogPost->toJson());
echo '<br>';
echo '<br>';
echo Film::getClassName();
echo '<br>';
echo BlogEntry::getClassName();
echo '<br>';
echo $blogPost->getMyName();
echo '<br>';
echo $blogPost->check();
echo '<br>';
echo $film->check();
die();
