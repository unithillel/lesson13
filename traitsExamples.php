<?php

trait Greatable{
    public function sayHello(){
        return 'hi ';
    }

    public function inspire(){
        return 'Some great quote here!';
    }
}

trait SayWorld{
    public function sayWorld(){
        return 'world!';
    }
    public function inspire(){
        return 'Do something cool!';
    }

    public function great(){
        return 'Hi ' . $this->getName();
    }

    abstract public function getName();
}

trait Check{
    public function check(){
        return 'check 2';
    }
}

class Entity{
    public function check(){
        return 'check 1';
    }
}

class Person extends Entity {
    use Greatable, SayWorld, Check{
        SayWorld::inspire insteadof Greatable;
    }

    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


}

$student = new Person('John');

echo $student->sayHello() . ' ' . $student->sayWorld();
echo '<br>';
echo $student->check();
echo '<br>';
echo $student->inspire();
echo '<br>';
echo $student->great();
