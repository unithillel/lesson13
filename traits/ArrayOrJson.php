<?php

trait ArrayOrJson{
    protected $myName = 'ArrayOrJson';

    public function getMyName(){
        return $this->myName;
    }

    public function toArray(){
        return get_object_vars($this);
    }

    public function toJson(){
        return json_encode($this->toArray());
    }

    static public function getClassName(){
        return __CLASS__;
    }

    public function check(){
        return 'I am from trait';
    }
}