<?php
require_once 'traits/ArrayOrJson.php';

class Film
{
    use ArrayOrJson;

    public $title;
    public $genre;
    public $rating;
    private $check = 'I am hidden';

    public function __construct($title, $genre, $rating)
    {
        $this->title = $title;
        $this->genre = $genre;
        $this->rating = $rating;
    }

}