<?php
require_once 'traits/ArrayOrJson.php';

class BlogEntry
{
    use ArrayOrJson;

    public $title;
    public $intro;
    public $content;

    public function __construct($title, $intro, $content)
    {
        $this->title = $title;
        $this->intro = $intro;
        $this->content = $content;
    }

    public function check(){
        return 'yo';
    }


}